package com.example.springjuinttest.controller;

import com.example.springjuinttest.entity.service.impl.StudentServiceImpl;
import com.example.springjuinttest.request.RequestStudent;
import com.example.springjuinttest.uat.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
public class StudentController {
    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
    @Autowired
    private StudentServiceImpl serviceImpl;
    @GetMapping("")
    public void getAllInfomation() {
        LOGGER.info( "=== [{}] StudentController getAllInfomation success :{}====");
        serviceImpl.getAll(new Student());
    }
    @PostMapping("/api/infomation")
    public void createInfomationIcNumber(String applyNo, String nationId, String no) throws Exception {
        LOGGER.info("=== [{}} StduentController createInfomationIcNumber post success :{} ===");
       serviceImpl.checkCondition(applyNo, nationId, no);
    };
}
