package com.example.springjuinttest.utils;

public interface ErrorCode {
    String IC_NUMBER_ALREADY_EXIST = "HYD-10-15-error";
    String INFOMATION_EXISTS = "Thông tin đã tồn tại";
    String DROP_CUSTOMER_FAILED = "Khách hàng thất bại";
    String ERROR_SERVER = "500";
    String NOT_FOUND = "404";
    String DELAY_RUNTIME = "1";
}
