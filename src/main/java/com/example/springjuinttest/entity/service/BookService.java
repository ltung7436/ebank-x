package com.example.springjuinttest.entity.service;

import com.example.springjuinttest.entity.Bock;

import java.util.List;

public interface BookService {
    List<Bock> getAll();
    Bock getOne(Long id);
}
