package com.example.springjuinttest.entity.service.impl;

import com.example.springjuinttest.entity.repository.StudentRepository;
import com.example.springjuinttest.entity.service.StudentService;
import com.example.springjuinttest.request.RequestStudent;
import com.example.springjuinttest.uat.Student;
import com.example.springjuinttest.utils.ConstantUtils;
import com.example.springjuinttest.utils.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);
    @Autowired
    private StudentRepository studentRepository;
    @Override
    public List<Student> getAll(Student student) {
        LOGGER.info("=== [{}] StudentServiceImpl processCheck infomation success ===");
        return studentRepository.findAll();
    }

    @Override
    public Student checkCondition(String fullName, String icNumber, String OldIcNumber) throws Exception {
        Student student = new Student();
        try {
            RequestStudent requestStudent = new RequestStudent();
            requestStudent.setFullName(fullName);
            requestStudent.setIcNumber(icNumber);
            requestStudent.setOldIcNumber(OldIcNumber);
            studentRepository.save(student);
            if (student.equals(ConstantUtils.RCODE00)) {
                LOGGER.info("=== [{}] StudentServiceImpl checkCondition success : {} ===", requestStudent);
            }
        }catch (Exception e) {
            LOGGER.error("=== [{}] StudentServiceImpl checkCondition error with message: {}===", e );
            throw new Exception(ErrorCode.IC_NUMBER_ALREADY_EXIST);
        }

        return student ;
    }
}
