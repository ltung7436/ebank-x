package com.example.springjuinttest.entity.service;

import com.example.springjuinttest.uat.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAll(Student student);
    Student checkCondition(String fullName, String icNumber, String OldIcNumber) throws Exception;
}
