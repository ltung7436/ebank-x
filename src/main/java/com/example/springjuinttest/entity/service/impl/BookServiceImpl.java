package com.example.springjuinttest.entity.service.impl;

import com.example.springjuinttest.entity.Bock;
import com.example.springjuinttest.entity.exception.BookNotFoundException;
import com.example.springjuinttest.entity.repository.BookRepository;
import com.example.springjuinttest.entity.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<Bock> getAll() {
        return bookRepository.findAll();
    }

    @Override
    public Bock getOne(Long id) {
        return bookRepository.findById(id).orElseThrow(BookNotFoundException::new);
    }
}
