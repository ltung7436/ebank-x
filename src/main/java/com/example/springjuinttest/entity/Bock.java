package com.example.springjuinttest.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "`PayTest`")
@Data
public class Bock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    private String name;
    private String address;

}
