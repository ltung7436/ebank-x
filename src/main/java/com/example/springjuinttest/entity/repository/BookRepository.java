package com.example.springjuinttest.entity.repository;

import com.example.springjuinttest.entity.Bock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Bock , Long> {
}
