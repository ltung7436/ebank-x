package com.example.springjuinttest.entity.repository;

import com.example.springjuinttest.uat.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
