package com.example.springjuinttest.request;

import lombok.Data;

@Data
public class RequestStudent {
    private String fullName;
    private String IcNumber;
    private String OldIcNumber;
}
