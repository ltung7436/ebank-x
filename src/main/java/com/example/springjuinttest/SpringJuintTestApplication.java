package com.example.springjuinttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJuintTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJuintTestApplication.class, args);
    }

}
